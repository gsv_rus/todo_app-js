(function () {

  let listArray = []
  let listName = ''
  //создаем и возвращаем заголоавок приложения
  function createAppTitle(title) {
    let appTitle = document.createElement('h2'); //создаем
    appTitle.innerHTML = title; // присваиваем
    return appTitle //возвращаем
  }

  // создаем и возвращаем форму для создания тела
  function createTodoItemForm() {
    let form = document.createElement('form'); //создаем элемент формы
    let input = document.createElement('input'); // создаем поле для ввода
    let buttonWrapper = document.createElement('div'); //создаем блок для правильной стилизации кнопки
    let button = document.createElement('button') // создаем  саму кнопку
    // расставим различные атрибуты нашим элементам
    form.classList.add('input-group', 'mb-3');// для формы устанавливаем два класса
    input.classList.add('form-control'); // для поля ввода устанавливаем класс, чтобы bootstrap правильно его отобразил
    input.placeholder = 'Введите название нового дела'; // placeholder
    buttonWrapper.classList.add('input-group-append'); // для позиционирования кнопки
    button.classList.add('btn', 'btn-primary'); // стили для кнопки
    button.textContent = 'Добавить дело'; // текст кнопки
    button.disabled = true;
    // отключаем кнопку если поле пустое
    input.addEventListener("input", function () {
      if (input.value != '') {
        button.disabled = false;
      } else {
        button.disabled = true;
      }
    });

    //объединяем DOM элементы в единую структуру
    buttonWrapper.append(button);
    form.append(input);
    form.append(buttonWrapper);

    return {
      form,
      input,
      button,
    };
  }

  //создаем и возвращаем список элементов
  function createTodoList() {
    let list = document.createElement('ul'); // присваиваем переменной элемент
    list.classList.add('list-group'); // стили для списка
    return list;
  }

  function createTodoItem(obj) {
    let item = document.createElement('li');
    // кнопки помещаем в элемент, который красиво покажет их в одной группе
    let buttonGroup = document.createElement('div');
    let doneButton = document.createElement('button');
    let deleteButton = document.createElement('button');

    //устванавливаем стили для элемента списка, а так же для размещения кнопок в рпавой части с помощью flex
    item.classList.add('list-group-item', 'd-flex', 'justify-content-between', 'align-items-center');
    item.textContent = obj.name; //могут быть спецсимволы, поэтому textContent, а не innerHtml

    buttonGroup.classList.add('btn-group', 'btn-group-sm');
    doneButton.classList.add('btn', 'btn-success')
    doneButton.textContent = 'Готово';
    deleteButton.classList.add('btn', 'btn-danger')
    deleteButton.textContent = 'Удалить';

    if (obj.done == true) item.classList.add('list-group-item-success')
    //добавляем обработчик на кнопки
    doneButton.addEventListener('click', function () {
      item.classList.toggle('list-group-item-success');
      for (const listItem of listArray) {
        if (listItem.id == obj.id) listItem.done = !listItem.done
      }

      saveList(listArray, listName)
    });
    deleteButton.addEventListener('click', function () {
      if (confirm('Are you sure about that?')) {
        item.remove();
        // удаляем элемент и из локального хранилища
        for (let i = 0; i < listArray.length; i++) {
          if (listArray[i].id == obj.id) listArray.splice(i, 1)
        }

        saveList(listArray, listName)
      }
    });

    // вкладываем кнопки в отдельный элемент, чтобы обработать события нажатия
    buttonGroup.append(doneButton);//вкладываем группу кнопок
    buttonGroup.append(deleteButton);
    item.append(buttonGroup);//саму группу кнопок вкладываем в item (li)

    //приложени. нужен доступ к самому элементу и кнопкам, чтобы обрабатывать события нажатия
    return {
      item,
      doneButton,
      deleteButton,
    };
  }
  // генерируем уникальный id
  function getNewID(arr) {
    let max = 0;
    for (const item of arr) {
      if (item.id > max) max = item.id
    }
    return max + 1;
  }
  // преобразовываем массив в строку
  function saveList(arr, keyName) {
    localStorage.setItem(keyName, JSON.stringify(arr));
  }

  function createTodoApp(container, title = 'Список дел', keyName) {

    let todoAppTitle = createAppTitle(title); // вызываем все 3 функции
    let todoItemForm = createTodoItemForm();
    let todoList = createTodoList();

    listName = keyName;

    container.append(todoAppTitle); // результат функций размещаем внутри контейнера
    container.append(todoItemForm.form);
    container.append(todoList);
    // получаем и сохраняем содержание локального хранилища
    let localData = localStorage.getItem(listName)

    if (localData !== null && localData !== '') listArray = JSON.parse(localData)

    for (const itemList of listArray) {
      let todoItem = createTodoItem(itemList);
      todoList.append(todoItem.item);
    }
    //браузер создает событие submit на форме по нажатию на Enter или на кнопку создания дела
    todoItemForm.form.addEventListener('submit', function (e) {
      //эта строчка необходима, чтобы предотвратить снандартные действия браузера
      //в данном случае мы не хотим, чтобы страница перезагружалась при отправке формы
      e.preventDefault();

      //игнорируем создание элемента, если пользователь ничего не ввел в поле
      if (!todoItemForm.input.value) {
        return;
      }

      let newItem = {
        id: getNewID(listArray),
        name: todoItemForm.input.value,
        done: false
      }

      let todoItem = createTodoItem(newItem);

      listArray.push(newItem)

      saveList(listArray, listName)
      //создаем и добаляем в список новое дело с названием из поля для ввода
      todoList.append(todoItem.item);
      todoItemForm.button.disabled = true;
      //обнуляем значение в поле, чтобы не пришлось стирать его вручную
      todoItemForm.input.value = '';
    });
  }

  window.createTodoApp = createTodoApp;
})();
